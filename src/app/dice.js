angular.module('dice', [])

.factory('DiceService', function() {

	var DiceService = (function () {
		
		var die = {
			one: 0, 
			two: 0
		};

		// A very minimalistic dice roll random engine
		var getRandomNumber = function(){
			return Math.ceil((Math.random() * 0.6) * 10); 
		}

		var playDice = function(){			
			die.one = getRandomNumber();
			die.two = getRandomNumber();					 
		}

		var getDice = function(){
			return die;
		}

		return {
			play : playDice,
			getDice : getDice
		}
	})();

	return (DiceService);
})

.controller('diceCtrl', ['$scope', 'DiceService', function($scope, DiceService){
	
	$scope.rollDice = function(){
		DiceService.play();
		var die = DiceService.getDice();
		$scope.dieOne = die.one;
		$scope.dieTwo = die.two;
	};

}]);