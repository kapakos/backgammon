angular.module('player', []).

factory('PlayerService', function() {
	var checkers = [],
		firstname = '',
		lastname = '',
		username = '';

	return {
		setFirstName: function(firstname) {
			this.firstname = firstname;
		},
		getFirstName: function() {
			return this.firstname;
		},

		setLastName: function(lastname) {
			this.lastname = lastname;
		},
		getLastName: function() {
			return this.lastname;
		},

		setUserName: function(username) {
			this.username = username;
		},
		getUserName: function() {
			return this.username;
		}
	};
});