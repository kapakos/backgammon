angular.module('checker', ['dice', 'dummyChecker'])

.directive('bgChecker',['$document', 'DiceService', function($document, DiceService) {
	return {
		restrict: 'A',
		replace: true,	
		scope: {},
		link: function(scope, element, attr) {
				
			function getCurrentRoll(){
				return DiceService.getDice();
			};

			element.on('mouseenter', function(event) {
				
			});

			element.on('mouseleave', function(event){
			
			});

			function displayPossiblePlay(offset){
				var val = 'px 0px  0px 0px rgba(231, 231, 47, 0.6)';			
			};

			function init(){
				var checkerFieldNumber = attr.checkerField,
				field = angular.element('[data-field-number='+ checkerFieldNumber +']').get(0),
				fieldRow = parseInt(field.dataset.rowNumber);
				position = {};				


				if(checkerFieldNumber < 12){
					position.bottom = parseInt(field.style.bottom) + (fieldRow*40) + 'px';
					position.left = field.style.left;
				
				} else {
					position.top = parseInt(field.style.top) + (fieldRow*40) + 'px';
					position.right = field.style.right;					
				}

				element.css(position);

				setFieldRowNumber(field, fieldRow);
			};

			function setFieldRowNumber(field, fieldRowNumber){				
				fieldRowNumber++;
				field.dataset.rowNumber = fieldRowNumber;
			}

			init();
			
		}
	};
}]);