'use strict';

var BackgammonApp = angular.module('backgammonApp', ['gameBoard','field', 'checker']).

config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.html5Mode(true);
	}
]).

controller('messageCtrl', ['$scope', 'BoardService',
	function($scope, BoardService) {
		BoardService.setStatus('start');
		$scope.statusMessage = 'Der Status des Spiels ist: ' + BoardService.getStatus();
	}
]).

controller('backgammonCtrl', ['$scope',
	function($scope) {
		
	}
])
;