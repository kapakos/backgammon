'use strict';

angular.module('gameBoard', []).

factory('BoardService', function() {

	var fields = [],
		fieldsCount = 24,
		status;

	var initBoard = function() {
		for (var i = 0; i < fieldsCount; i++) {
			this.fields.push({
				rowsTaken: 0,
				fieldNumber: i
			});
		};
	};

	return {
		initBoard: initBoard,
		setStatus: function(status) {
			this.status = status;
		},
		getStatus: function() {
			return this.status;
		}
	};
}).

controller('boardCtrl', ['$scope', 'BoardService',
	function($scope, BoardService) {

		BoardService.setStatus('start');

		$scope.status = BoardService.getStatus();
	}
])

;