var express = require('express'),
    routes = require('./routes'),
    path = require('path'),
    logger = require('morgan'),
    app = express();


/* Configuration */

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, '../src/views'));
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, '../src')));

// Routes
app.get('/', routes.index);
app.get('partial/:name', routes.partial);
app.get('*', routes.index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
