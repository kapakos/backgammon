var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    prefix = require('gulp-autoprefixer'),
    karma = require('gulp-karma');


// Defines path to sass
var sassRoot = 'src/sass/',
    cssRoot = 'src/css';

var testFiles = [
  'src/bower_components/angular/angular.js',
  'src/bower_components/angular-mocks/angular-mocks.js',
  'src/app/**/*.js',
  'test/**/*Spec.js'
];

// Gulp task
gulp.task('sass', function(){
  return gulp.src(sassRoot + 'main.scss')
    .pipe(sass({sourcemap: true}))
    .pipe(prefix("last 3 versions"))
    .pipe(gulp.dest(cssRoot));
});

gulp.task('watch-sass', function(){
  gulp.watch(sassRoot + '**/*.scss', function(){
    gulp.run('sass');
  });
});

gulp.task('karma-runner', function(){
  return gulp.src(testFiles)
    .pipe(karma({
      configFile: 'karma.conf.js',
      action: 'watch'
    }))
    .on('error', function(err){
      throw err;
    });
});

gulp.task('default', function(){
  gulp.run('sass');
  gulp.run('watch-sass');
  gulp.run('karma-runner');
});

