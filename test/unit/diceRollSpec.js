describe('dice roll service test', function() {
	var diceService, $injector;

	beforeEach(function() {
		$injector = angular.injector(['dice']);
		diceService = $injector.get('diceService');
	});

	describe('when I call rollDice', function() {

		it('returns a number between 1 and 6', function() {
			var diceRoll = diceService.play();

			expect(diceRoll.one).toBeGreaterThan(0, 'was ' + diceRoll.one);
			expect(diceRoll.two).toBeGreaterThan(0, 'was ' + diceRoll.two);
			expect(diceRoll.one).toBeLessThan(7);
			expect(diceRoll.two).toBeLessThan(7);
		});
	});

	describe('when I roll the dice 1000 times', function() {
		it('is evenly distributed', function() {
			var randomNumber = function() {
				return Math.ceil((Math.random() * 0.6) * 10);
			};

			var rolls = {
				ones: [],
				twos: [],
				threes: [],
				fours: [],
				fives: [],
				sixes: []
			};


			for (var i = 0; i < 100; i++) {
				var nr = randomNumber();

				if (nr === 1) {
					rolls.ones.push(nr);

				} else if (nr === 2) {
					rolls.twos.push(nr);

				} else if (nr === 3) {
					rolls.threes.push(nr);

				} else if (nr === 4) {
					rolls.fours.push(nr);

				} else if (nr === 5) {
					rolls.fives.push(nr);

				} else if (nr === 6) {
					rolls.sixes.push(nr);

				} else {
					console.log('this number is not allowed: ' + nr);
				}
			}


			console.log('ones: ' + rolls.ones.length);
			console.log('twos: ' + rolls.twos.length);
			console.log('threes: ' + rolls.threes.length);
			console.log('fours: ' + rolls.fours.length);
			console.log('fives: ' + rolls.fives.length);
			console.log('sixes: ' + rolls.sixes.length);
		});
	});

});